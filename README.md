# Xendit

## Getting Started

### Prerequisites

* Node@14.x+
* MongoDB

### Installing

* Install dependencies

> $ npm install

> $ npm run prepare

* Install package dependencies

> $ npm run boostrap

## Running the tests

* Run tests

> $ npm run test

## Running the application

> $ npm start

## Generating docs

> $ npm run docs

## Built With

* antd@4.19.1
* eslint@8.10.0
* express@4.17.3
* husky@7.0.4
* jsdoc@3.6.10
* lerna@4.0.0
* lint-staged@12.3.5
* mongoose@6.2.4
* react@17.0.2
* react-router-dom@6.2.2
