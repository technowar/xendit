require('dotenv').config();

const Fs = require('fs');
const Path = require('path');
const Express = require('express');
const ExpressConfig = require('./app/config/ExpressConfig');
const RouteConfig = require('./app/config/RouteConfig');

/**
 * Returns Express application
 *
 * @return    {Object}     Express application
 */
exports.Server = () => {
  // Load models
  const Models = Path.join(__dirname, 'app/models');

  /**
   * Read models
   */
  Fs.readdirSync(Models)
    .filter((Model) => ~Model.search(/^[^.].*\.js$/))
    .forEach((Model) => require(Path.join(Models, Model)));

  const App = Express();

  /**
   * Load Express application configurations
   */
  ExpressConfig(App);

  /**
   * Load Express routes configuration
   */
  RouteConfig(App);

  return App;
};
