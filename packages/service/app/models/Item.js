const Mongoose = require('mongoose');

/**
 * Item schema
 */
const ItemSchema = new Mongoose.Schema({
  created: {
    default: Date.now,
    type: Date,
  },
  category: {
    type: Mongoose.Schema.Types.ObjectId,
    ref: 'Category',
  },
  make: {
    required: true,
    type: String,
  },
  price: {
    required: true,
    type: Number,
  },
  product: {
    required: true,
    type: String,
  },
  quantity: {
    required: true,
    type: Number,
  },
  upc: {
    required: true,
    type: Number,
  },
});

module.exports = Mongoose.model('Item', ItemSchema);
