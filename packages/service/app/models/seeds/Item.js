const Item = require('../Item');

exports.ItemSeed = async (categories) => {
  const [{ _id: phoneId }, { _id: tabletId }] = categories;

  await Item.create([{
    category: phoneId,
    make: 'Apple',
    price: 829,
    product: 'iPhone 13',
    quantity: 5,
    upc: 282333551201,
  }, {
    category: phoneId,
    make: 'Google',
    price: 599,
    product: 'Pixel 6',
    quantity: 3,
    upc: 890723187356,
  }, {
    category: phoneId,
    make: 'Samsung',
    price: 1299,
    product: 'Galaxy Note 20',
    quantity: 8,
    upc: 320752869035,
  }, {
    category: tabletId,
    make: 'Apple',
    price: 549,
    product: 'iPad',
    quantity: 10,
    upc: 157932959620,
  }, {
    category: tabletId,
    make: 'Amazon',
    price: 149,
    product: 'Fire HD 10',
    quantity: 4,
    upc: 939855941868,
  }, {
    category: tabletId,
    make: 'Samsung',
    price: 849,
    product: 'Galaxy Tab S7+',
    quantity: 2,
    upc: 591528030726,
  }]);
};
