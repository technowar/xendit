require('dotenv').config();

const { Connection } = require('../../../db');
const { CategorySeed } = require('./Category');
const { ItemSeed } = require('./Item');

async function Seed() {
  const db = await Connection();
  const categories = await CategorySeed();
  await ItemSeed(categories);

  db.disconnect();
}

Seed().catch((error) => console.log(error));
