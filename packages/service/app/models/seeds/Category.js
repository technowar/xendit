const Category = require('../Category');

exports.CategorySeed = async () => {
  const categories = await Category.create([{
    name: 'Phone',
  }, {
    name: 'Tablet',
  }]);

  return categories;
};
