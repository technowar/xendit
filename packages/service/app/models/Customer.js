const Mongoose = require('mongoose');

/**
 * Customer schema
 */
const CustomerSchema = new Mongoose.Schema({
  created: {
    default: Date.now,
    type: Date,
  },
  email: {
    required: true,
    type: String,
    unique: true,
  },
  name: {
    required: true,
    type: String,
  },
});

module.exports = Mongoose.model('Customer', CustomerSchema);
