const Mongoose = require('mongoose');

/**
 * Order schema
 */
const OrderSchema = new Mongoose.Schema({
  created: {
    default: Date.now,
    type: Date,
  },
  customer: {
    required: true,
    type: Mongoose.Schema.Types.ObjectId,
    ref: 'Customer',
  },
  orders: [{
    item: {
      required: true,
      type: Mongoose.Schema.Types.ObjectId,
      ref: 'Item',
    },
    order: {
      required: true,
      type: Number,
    },
  }],
  total: {
    required: true,
    type: Number,
  },
});

module.exports = Mongoose.model('Order', OrderSchema);
