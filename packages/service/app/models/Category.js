const Mongoose = require('mongoose');

/**
 * Category schema
 */
const CategorySchema = new Mongoose.Schema({
  created: {
    default: Date.now,
    type: Date,
  },
  name: {
    required: true,
    type: String,
  },
});

module.exports = Mongoose.model('Category', CategorySchema);
