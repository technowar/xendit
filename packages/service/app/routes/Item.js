const Mongoose = require('mongoose');
const Item = require('../models/Item');
const { filterQueryString } = require('../utils');

/**
 * Get item
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 */
async function GetItem(req, res) {
  try {
    const {
      ObjectId: { isValid },
    } = Mongoose.Types;
    const {
      params: { id },
    } = req;

    if (!isValid(id)) {
      const err = {
        code: 404,
        message: 'Item not found',
      };

      throw err;
    }

    const data = await Item.findById(req.params.id).populate('category').exec();

    res.status(200).json({
      data,
      status: 'success',
    });
  } catch (error) {
    const err = {
      code: error.code || 500,
      message: error.message || 'Something went wrong',
    };

    res.status(err.code).json({
      message: err.message,
      status: 'error',
    });
  }
}

/**
 * Get items
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 */
async function GetItems(req, res) {
  try {
    const { qs, sub } = filterQueryString(req.query, 'item');
    const data = await Item.find({
      ...qs,
      ...(Object.keys(sub).length && {
        category: { ...sub },
      }),
    }).populate('category').exec();

    res.status(200).json({
      data,
      status: 'success',
    });
  } catch (error) {
    const err = {
      code: error.code || 500,
      message: error.message || 'Something went wrong',
    };

    res.status(err.code).json({
      message: err.message,
      status: 'error',
    });
  }
}

module.exports = {
  GetItem,
  GetItems,
};
