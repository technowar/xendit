const Customer = require('../models/Customer');

/**
 * Authenticate customer
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 */
async function AuthenticateCustomer(req, res) {
  try {
    const data = await Customer.findOne(req.body).exec();

    if (!data) {
      const err = {
        code: 404,
        message: 'Customer not found',
      };

      throw err;
    }

    res.status(200).json({
      data,
      status: 'success',
    });
  } catch (error) {
    const err = {
      code: error.code || 500,
      message: error.message || 'Something went wrong',
    };

    res.status(err.code).json({
      message: err.message,
      status: 'error',
    });
  }
}

/**
 * Create customer
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 */
async function CreateCustomer(req, res) {
  try {
    const data = await Customer.create(req.body);

    res.status(201).json({
      data,
      status: 'success',
    });
  } catch (error) {
    const code = error.code || 500;
    const err = {
      code: code > 600 ? 400 : code,
      message: error.message || 'Something went wrong',
    };

    res.status(err.code).json({
      message: err.message,
      status: 'error',
    });
  }
}

module.exports = {
  AuthenticateCustomer,
  CreateCustomer,
};
