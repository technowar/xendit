const Order = require('../models/Order');
const { filterQueryString } = require('../utils');
const { FindOneAndUpdate } = require('../utils/Item');

/**
 * Create orders
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 */
async function CreateOrder(req, res) {
  try {
    const {
      body: {
        customer,
        items,
        total,
      },
    } = req;

    const data = await Order.create({
      customer,
      orders: items.map((item) => ({
        item: item.id,
        order: item.order,
      })),
      total,
    });

    await Promise.all(items.map(async (itemTemp) => FindOneAndUpdate(itemTemp)));

    res.status(201).json({
      data,
      status: 'success',
    });
  } catch (error) {
    const err = {
      code: error.code || 500,
      message: error.message || 'Something went wrong',
    };

    res.status(err.code).json({
      message: err.message,
      status: 'error',
    });
  }
}

/**
 * Get orders
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 */
async function GetOrders(req, res) {
  try {
    const { sub } = filterQueryString(req.query, 'order');
    const data = await Order.find({
      ...(Object.keys(sub).length && {
        customer: sub,
      }),
    }).populate('customer orders.item').exec();

    res.status(200).json({
      data,
      status: 'success',
    });
  } catch (error) {
    const err = {
      code: error.code || 500,
      message: error.message || 'Something went wrong',
    };

    res.status(err.code).json({
      message: err.message,
      status: 'error',
    });
  }
}

module.exports = {
  CreateOrder,
  GetOrders,
};
