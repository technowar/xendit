/**
 * Sets XSRF-TOKEN to cookie
 *
 * @param     {Object}      req     Express request
 * @param     {Object}      res     Express response
 * @param     {Function}    next    Express next
 */
function Loader(req, res, next) {
  res.cookie('XSRF-TOKEN', req.csrfToken());

  next();
}

module.exports = {
  Loader,
};
