const SUPPORTED_ITEM_QS = ['make'];
const SUPPORTED_ITEM_SUB_QS = ['category'];
const SUPPORTED_ORDER_SUB_QS = ['customer'];

/**
 * Returns constructed query strings
 *
 * @param     {Object}    queries     Query strings
 * @param     {String}    type        Endpoint identifier
 *
 * @returns   {Object}                Formatted query strings
 */
function filterQueryString(queries, type) {
  return Object.keys(queries).reduce((acc, curr) => {
    const key = curr.toLowerCase();
    const opts = {
      item: () => {
        if (SUPPORTED_ITEM_QS.some((item) => item === key)) {
          acc.qs = {
            [key]: queries[curr],
          };
        }

        if (SUPPORTED_ITEM_SUB_QS.some((item) => item === key)) {
          acc.sub = {
            _id: queries[curr],
          };
        }
      },
      order: () => {
        if (SUPPORTED_ORDER_SUB_QS.some((item) => item === key)) {
          acc.sub = {
            _id: queries[curr],
          };
        }
      },
    };

    opts[type]();

    return acc;
  }, {
    qs: {},
    sub: {},
  });
}

module.exports = {
  filterQueryString,
};
