const Item = require('../models/Item');

/**
 * Returns Item model
 *
 * @param     {Object}    item        Item detail
 *
 * @returns   {Promise}               Updated item detail
 */
async function FindOneAndUpdate(item) {
  const items = await Item.findOneAndUpdate({
    _id: item.id,
  }, {
    quantity: item.quantity,
  }, {
    new: true,
  }).populate('category');

  return items;
}

module.exports = {
  FindOneAndUpdate,
};
