const BodyParser = require('body-parser');
const Compression = require('compression');
const CookieParser = require('cookie-parser');
const Cors = require('cors');
const Csurf = require('csurf');
const { Loader } = require('../utils/Token');

/**
 * Express application configurations
 *
 * @param     {Object}    App     Express application
 */
module.exports = (App) => {
  const env = process.env.NODE_ENV === 'production';

  App.use(BodyParser.json());
  App.use(BodyParser.urlencoded({
    extended: true,
  }));
  App.use(Compression({
    level: 9,
    memLevel: 9,
  }));
  App.use(CookieParser());
  App.use(Cors({
    credentials: true,
    origin: true,
  }));
  App.use(Csurf({
    cookie: {
      domain: env ? '.herokuapp.com' : 'localhost',
      httpOnly: env,
      path: '/',
      secure: env,
    },
  }));
  App.use(Loader);
};
