const { AuthenticateCustomer, CreateCustomer } = require('../routes/Customer');
const { GetItem, GetItems } = require('../routes/Item');
const { CreateOrder, GetOrders } = require('../routes/Order');

/**
 * Express routes configuration
 *
 * @param     {Object}    App     Express application
 */
module.exports = (App) => {
  App.get('/items', GetItems);
  App.get('/items/:id', GetItem);
  App.get('/orders', GetOrders);
  App.post('/customers', CreateCustomer);
  App.post('/orders', CreateOrder);
  App.post('/signin', AuthenticateCustomer);
};
