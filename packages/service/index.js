const { Server } = require('./app');
const { Connection } = require('./db');

/**
 * Starts application
 */
async function Start() {
  const Port = process.env.PORT || 3030;
  const App = Server();

  await Connection();

  App.listen(Port, () => console.log(`Listening server to ${Port}`));
}

Start().catch((error) => console.log(error));
