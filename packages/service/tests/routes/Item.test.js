const { GetItem } = require('../../app/routes/Item');

describe('Item', () => {
  let mockedReq = {};
  let mockedRes = {};

  beforeEach(() => {
    mockedReq = {};
    mockedRes = {
      json: jest.fn(),
      status: jest.fn(() => mockedRes),
    };
  });

  test('item not found', async () => {
    mockedReq = {
      params: { _id: 1 },
    };

    await GetItem(mockedReq, mockedRes);

    expect(mockedRes.status).toHaveBeenCalledWith(404);
    expect(mockedRes.json).toHaveBeenCalledWith({ message: 'Item not found', status: 'error' });
  });
});
