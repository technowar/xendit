const Customer = require('../../app/models/Customer');

describe('Customer', () => {
  test('findOne', () => {
    const body = {
      email: 'a',
      name: 'a',
    };
    const data = {
      email: 'a',
      name: 'a',
    };
    const spy = jest.spyOn(Customer, 'findOne').mockReturnValueOnce(data);

    Customer.findOne(body);

    const mockedSpy = spy.mock.results[0].value;

    expect(spy).toHaveBeenCalledTimes(1);
    expect(mockedSpy.name).toEqual(data.name);

    spy.mockReset();
  });
});
