const Order = require('../../app/models/Order');

describe('Order', () => {
  test('findOne', () => {
    const query = { customer: '622aeaeb025ba8bd4f5dc286' };
    const data = {
      customer: {},
      orders: [],
      total: 1200,
    };
    const spy = jest.spyOn(Order, 'find').mockReturnValueOnce(data);

    Order.find(query);

    const mockedSpy = spy.mock.results[0].value;

    expect(spy).toHaveBeenCalledTimes(1);
    expect(mockedSpy.total).toEqual(data.total);

    spy.mockReset();
  });
});
