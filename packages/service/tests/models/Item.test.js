const Item = require('../../app/models/Item');

describe('Item', () => {
  test('findById', () => {
    const _id = '6228b1cb18f64312b7a8ca0e';
    const data = {
      make: 'Apple',
      price: 829,
      product: 'iPhone 13',
      quantity: 5,
    };
    const spy = jest.spyOn(Item, 'findById').mockReturnValueOnce(data);

    Item.findById({ _id });

    const mockedSpy = spy.mock.results[0].value;

    expect(spy).toHaveBeenCalledTimes(1);
    expect(mockedSpy.make).toEqual(data.make);

    spy.mockReset();
  });
});
