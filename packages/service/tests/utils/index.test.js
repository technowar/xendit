const { filterQueryString } = require('../../app/utils');

describe('filterQueryString', () => {
  test('format query strings for item', () => {
    const { qs, sub } = filterQueryString({
      category: '6228b1cb18f64312b7a8ca0a',
      make: 'Apple',
    }, 'item');

    expect(qs).toEqual({ make: 'Apple' });
    expect(sub).toEqual({ _id: '6228b1cb18f64312b7a8ca0a' });
  });

  test('format query strings for order', () => {
    const { qs, sub } = filterQueryString({
      customer: '622aeaeb025ba8bd4f5dc286',
    }, 'order');

    expect(qs).toEqual({});
    expect(sub).toEqual({ _id: '622aeaeb025ba8bd4f5dc286' });
  });

  test('empty objects on unsupported query strings', () => {
    const { qs, sub } = filterQueryString({});

    expect(qs).toEqual({});
    expect(sub).toEqual({});
  });
});
