const { Loader } = require('../../app/utils/Token');

describe('XSRF-TOKEN', () => {
  let mockedReq = {};
  let mockedRes = {};
  let mockedNext = {};

  beforeEach(() => {
    mockedReq = {
      csrfToken: jest.fn(),
    };
    mockedRes = {
      cookie: jest.fn(),
    };
    mockedNext = jest.fn();
  });

  test('cookie has been set with XSRF-TOKEN', () => {
    mockedReq.csrfToken.mockReturnValue(1234);

    Loader(mockedReq, mockedRes, mockedNext);

    expect(mockedRes.cookie).toHaveBeenCalledWith('XSRF-TOKEN', 1234);
  });
});
