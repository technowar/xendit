const Mongoose = require('mongoose');

/**
 * Returns MongoDB connection
 *
 * @return {Promise}    MongoDB connection
 */
exports.Connection = async () => {
  const MongoURL = process.env.MONGO_URL || 'mongodb://127.0.0.1/xendit';
  const MongooseConnection = await Mongoose.connect(MongoURL, {
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    useNewUrlParser: true,
  });

  return MongooseConnection;
};
