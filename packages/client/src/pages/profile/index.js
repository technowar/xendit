import React, {
  Suspense,
  lazy,
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  Button,
  Col,
  Row,
  Typography,
} from 'antd';
import { Navigate, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getOrders } from 'services/order';
import { removeLocalStorage } from 'utils';
import './styles.css';

const List = lazy(() => import('components/list'));

/**
 * Component for profile page
 *
 * @component
 * @property    {Object}      customer            Customer details
 * @property    {Boolean}     isAuthenticated     Customer authentication status
 */
function ProfilePage(props) {
  const { customer, isAuthenticated } = props;

  /**
   * Customer checking authentication
   *
   * Redirects unauthorized customers to signin page
   */
  if (!isAuthenticated) {
    return <Navigate replace to="/signin" />;
  }

  const navigate = useNavigate();
  const [{
    listItem,
    loading,
  }, setState] = useState({
    listItem: [],
    loading: false,
  });

  /**
   * Handles fetching of customer orders
   *
   * Sets `listItem` local state to order details
   *
   * Redirects customer to home page on error
   */
  const handleFetchContent = useCallback(async () => {
    try {
      setState((prevState) => ({
        ...prevState,
        loading: true,
      }));

      const fetchedOrders = await getOrders({ customer: customer._id });

      setState((prevState) => ({
        ...prevState,
        listItem: fetchedOrders,
      }));
    } catch (err) {
      navigate('/');
    } finally {
      setState((prevState) => ({
        ...prevState,
        loading: false,
      }));
    }
  }, []);

  /**
   * Handles sign out button click
   *
   * Removes `cart` local storage
   *
   * Removes `user` local storage
   *
   * Redirects unauthorized customers to home page
   */
  function handleClickSignout() {
    removeLocalStorage('cart');
    removeLocalStorage('user');

    navigate('/', { replace: true });
  }

  /**
   * Call `handleFetchContent` when component is mounted
   */
  useEffect(() => {
    handleFetchContent();
  }, [handleFetchContent]);

  return (
    <div className="profile">
      <Row gutter={16}>
        <Col
          md={{ offset: 8, span: 8 }}
          xs={24}
        >
          <div className="profile__list">
            <Typography.Title className="profile__list-title" level={3}>
              Orders
            </Typography.Title>
            <Suspense fallback={null}>
              <List
                list={listItem}
                loading={loading}
                type="orders"
              />
            </Suspense>
            <div className="profile__action">
              <Button className="profile__action-btn" onClick={() => handleClickSignout()}>
                SIGN OUT
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

ProfilePage.propTypes = {
  /**
   * Customer details
   */
  customer: PropTypes.instanceOf(Object).isRequired,
  /**
   * Customer authentication status
   */
  isAuthenticated: PropTypes.bool.isRequired,
};

export default ProfilePage;
