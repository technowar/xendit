import React, { useState } from 'react';
import {
  Button,
  Col,
  Input,
  Row,
  Space,
  Typography,
} from 'antd';
import { useNavigate } from 'react-router-dom';
import { UseStateValue } from 'provider';
import { authenticateCustomer } from 'services/customer';
import { setLocalStorage } from 'utils';
import './styles.css';

/**
 * Component for sign in page
 *
 * @component
 */
function SigninPage() {
  const [{
    email,
    name,
  }, setState] = useState({
    email: null,
    name: null,
  });
  const navigate = useNavigate();
  const [, dispatch] = UseStateValue();

  /**
   * Handles sign in button click
   *
   * Sets `user` local storage to customer details
   *
   * Redirects customer to home page
   *
   * Triggers alert reducer on error
   */
  async function handleClickSignin() {
    try {
      const user = await authenticateCustomer({ email, name });

      setLocalStorage('user', user);

      navigate('/');
    } catch (err) {
      dispatch({
        type: 'ALERT_DISPATCH',
        payload: {
          message: 'Please check credentials',
          type: 'error',
        },
      });
    }
  }

  /**
   * Handles input change
   *
   * Sets local states to its corresponding value
   */
  function handleChangeInput(evt, type) {
    evt.persist();

    setState((prevState) => ({
      ...prevState,
      [type]: evt.target.value,
    }));
  }

  return (
    <div className="signin">
      <Row gutter={16}>
        <Col
          md={{ offset: 8, span: 8 }}
          xs={24}
        >
          <div className="signin__form">
            <Typography.Title className="signin__form-title" level={3}>
              Sign In
            </Typography.Title>
            <Space className="signin__form--fields" direction="vertical">
              <Input
                onChange={(evt) => handleChangeInput(evt, 'email')}
                placeholder="Email address"
                value={email}
              />
              <Input
                onChange={(evt) => handleChangeInput(evt, 'name')}
                placeholder="Full name"
                value={name}
              />
            </Space>
            <div className="signin__action">
              <Button
                className="signin__action-btn"
                onClick={() => handleClickSignin()}
                type="primary"
              >
                SIGN IN
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default SigninPage;
