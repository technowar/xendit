import React, {
  Suspense,
  lazy,
  useEffect,
  useState,
} from 'react';
import {
  Col,
  Divider,
  Input,
  Row,
  Space,
} from 'antd';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { UseStateValue } from 'provider';
import './styles.css';

const Cart = lazy(() => import('components/cart'));

/**
 * Component for checkout page
 *
 * @component
 * @property    {Object}      customer            Customer details
 * @property    {Boolean}     isAuthenticated     Customer authentication status
 * @property    {Number}      size                Cart size
 */
function CheckoutPage(props) {
  const {
    customer,
    isAuthenticated,
    size,
  } = props;

  /**
   * Customer checking authentication and cart size
   *
   * Redirects unauthorized customers and empty cart to signin page
   */
  if (!isAuthenticated && !size) {
    return <Navigate replace to="/" />;
  }

  const [{
    email,
    name,
  }, setState] = useState({
    email: null,
    name: null,
  });
  const [, dispatch] = UseStateValue();

  /**
   * Handles input change
   *
   * Sets local states to its corresponding value
   */
  function handleChangeInput(evt, type) {
    evt.persist();

    setState((prevState) => ({
      ...prevState,
      [type]: evt.target.value,
    }));
  }

  /**
   * Triggers checkout reducer when component is mounted
   */
  useEffect(() => {
    dispatch({
      type: 'CHECKOUT_DISPATCH',
      payload: { payment: true },
    });
  }, []);

  return (
    <div className="checkout">
      <Row gutter={16}>
        <Col
          md={{ offset: 8, span: 8 }}
          xs={24}
        >
          {!isAuthenticated && (
            <div className="checkout__customer">
              <Space className="checkout__customer--field" direction="vertical">
                <Input
                  onChange={(evt) => handleChangeInput(evt, 'email')}
                  placeholder="Email address"
                  value={email}
                />
                <Input
                  onChange={(evt) => handleChangeInput(evt, 'name')}
                  placeholder="Full name"
                  value={name}
                />
              </Space>
              <Divider className="checkout__customer--divider" />
            </div>
          )}
          <Suspense fallback={null}>
            <Cart
              isAuthenticated={isAuthenticated}
              user={isAuthenticated ? customer : { email, name }}
            />
          </Suspense>
        </Col>
      </Row>
    </div>
  );
}

CheckoutPage.propTypes = {
  /**
   * Customer details
   */
  customer: PropTypes.instanceOf(Object),
  /**
   * Customer authentication status
   */
  isAuthenticated: PropTypes.bool.isRequired,
  /**
   * Cart size
   */
  size: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]).isRequired,
};

CheckoutPage.defaultProps = {
  customer: {},
};

export default CheckoutPage;
