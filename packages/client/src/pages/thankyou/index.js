import React, { useEffect } from 'react';
import {
  Col,
  Row,
  Typography,
} from 'antd';
import { Navigate, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { removeLocalStorage } from 'utils';
import './styles.css';

/**
 * Component for thank you page
 *
 * @component
 * @property    {Boolean}     isAuthenticated     Customer authentication status
 */
function ThankYouPage(props) {
  const { isAuthenticated } = props;

  /**
   * Customer checking authentication
   *
   * Redirects unauthorized customers to home page
   */
  if (!isAuthenticated) {
    return <Navigate replace to="/" />;
  }

  const navigate = useNavigate();

  /**
   * Handles the redirection to home page
   */
  function handleClickLink() {
    navigate('/', { replace: true });
  }

  /**
   * Removes `cart` local storage when component is mounted
   */
  useEffect(() => {
    removeLocalStorage('cart');
  }, []);

  return (
    <div className="thankyou">
      <Row gutter={16}>
        <Col
          md={{ offset: 8, span: 8 }}
          xs={24}
        >
          <div className="thankyou__info">
            <Typography.Title level={3}>
              Thank you for your purchase.
            </Typography.Title>
            <div>
              <Typography.Text>
                {'You can continue shopping by clicking '}
              </Typography.Text>
              <Typography.Link onClick={() => handleClickLink()}>
                here
              </Typography.Link>
              <Typography.Text>
                .
              </Typography.Text>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

ThankYouPage.propTypes = {
  /**
   * Customer authentication status
   */
  isAuthenticated: PropTypes.bool.isRequired,
};

export default ThankYouPage;
