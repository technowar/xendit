import React, {
  Suspense,
  lazy,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { ShoppingCartOutlined } from '@ant-design/icons';
import {
  Avatar,
  Button,
  Card,
  Col,
  Row,
  Typography,
} from 'antd';
import { useNavigate, useParams } from 'react-router-dom';
import { UseStateValue } from 'provider';
import { getItem, getItems } from 'services/item';
import { formatCurrency } from 'utils';
import './styles.css';

const List = lazy(() => import('components/list'));

/**
 * Component for item page
 *
 * @component
 */
function ItemPage() {
  const [{
    item,
    listItem,
    loading,
  }, setState] = useState({
    item: {
      make: null,
      price: 0,
      product: null,
      quantity: 0,
    },
    listItem: [],
    loading: false,
  });
  const navigate = useNavigate();
  const { id } = useParams();
  const [, dispatch] = UseStateValue();
  const price = formatCurrency(item.price, 'USD');

  /**
   * Handles fetching of items
   *
   * Sets `item` local state to specific item detail
   *
   * Sets `listItem` local state to item details
   *
   * Redirects customer to home page on error
   */
  const handleFetchContent = useCallback(async () => {
    try {
      setState((prevState) => ({
        ...prevState,
        loading: true,
      }));

      const fetchedItem = await getItem(id);
      const fetchedItems = await getItems({ category: fetchedItem.category._id });

      setState((prevState) => ({
        ...prevState,
        item: fetchedItem,
        listItem: fetchedItems.filter((itemTemp) => itemTemp._id !== id),
      }));
    } catch (err) {
      navigate('/');
    } finally {
      setState((prevState) => ({
        ...prevState,
        loading: false,
      }));
    }
  }, [id]);

  /**
   * Handles button click when item is added to cart
   *
   * Triggers alert reducer when adding
   *
   * Triggers cart reducer when adding
   */
  function handleClickButton() {
    const {
      created,
      upc,
      ...rest
    } = item;

    dispatch({
      type: 'ALERT_DISPATCH',
      payload: {
        message: `${item.make} ${item.product} added to cart`,
        type: 'info',
      },
    });
    dispatch({
      type: 'CART_ADD',
      payload: { item: rest },
    });
  }

  /**
   * Call `handleFetchContent` when component is mounted
   */
  useEffect(() => {
    handleFetchContent();
  }, [handleFetchContent]);

  return (
    <div className="item">
      <Row gutter={16}>
        <Col md={16} xs={24}>
          <div className="item__info">
            <Card
              className="item__info--card"
              cover={(
                <img
                  alt="example"
                  src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                />
              )}
            >
              <Card.Meta
                avatar={
                  <Avatar src="https://joeschmoe.io/api/v1/random" />
                }
                className="item__info--card-meta"
                description={(
                  <div className="item__info--card-meta-description">
                    <div className="item__info--card-meta-description-details">
                      <Typography.Text>
                        {'Make: '}
                      </Typography.Text>
                      <Typography.Text strong>
                        {item.make}
                      </Typography.Text>
                    </div>
                    <div className="item__info--card-meta-description-details">
                      <Typography.Text>
                        {'Price: '}
                      </Typography.Text>
                      <Typography.Text strong>
                        {price}
                      </Typography.Text>
                    </div>
                    <div className="item__info--card-meta-description-details">
                      <Typography.Text>
                        {'Stock: '}
                      </Typography.Text>
                      <Typography.Text strong>
                        {item.quantity}
                      </Typography.Text>
                    </div>
                    <div className="item__info--card-meta-description-btn">
                      <Button
                        disabled={!item.quantity}
                        icon={<ShoppingCartOutlined />}
                        onClick={() => handleClickButton()}
                        shape="round"
                        size="small"
                        type="default"
                      />
                    </div>
                  </div>
                )}
                title={(
                  <Typography.Text strong>
                    {item.product}
                  </Typography.Text>
                )}
              />
            </Card>
          </div>
        </Col>
        <Col md={8} xs={24}>
          <div className="item__similar">
            <div className="item__similar--title">
              <Typography.Title className="item__similar--title-text" level={3}>
                Similar
              </Typography.Title>
            </div>
            <div className="item__similar--list">
              <Suspense fallback={null}>
                <List
                  list={listItem}
                  loading={loading}
                  type="items"
                />
              </Suspense>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default ItemPage;
