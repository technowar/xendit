import React, {
  Suspense,
  lazy,
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  Col,
  Row,
  Typography,
} from 'antd';
import PropTypes from 'prop-types';
import Filter from 'components/filter';
import { UseStateValue } from 'provider';
import { getItems } from 'services/item';
import './styles.css';

const Cart = lazy(() => import('components/cart'));
const List = lazy(() => import('components/list'));

/**
 * Component for landing page
 *
 * @component
 */
function IndexPage(props) {
  const { isAuthenticated } = props;
  const [{
    listFilter,
    listItem,
    loading,
  }, setState] = useState({
    listFilter: [],
    listItem: [],
    loading: false,
  });
  const [{
    filter: { name: filterName, type: filterType },
  }, dispatch] = UseStateValue();

  /**
   * Handles fetching of items
   *
   * Sets `listFilter` local state to item details
   *
   * Sets `listItem` local state to item details
   *
   * Triggers alert reducer on error
   */
  const handleFetchContent = useCallback(async () => {
    try {
      setState((prevState) => ({
        ...prevState,
        loading: true,
      }));

      const fetchedItems = await getItems();

      setState((prevState) => ({
        ...prevState,
        listFilter: fetchedItems,
        listItem: fetchedItems,
      }));
    } catch (err) {
      dispatch({
        type: 'ALERT_DISPATCH',
        payload: {
          message: 'Unable to fetch items',
          type: 'error',
        },
      });
    } finally {
      setState((prevState) => ({
        ...prevState,
        loading: false,
      }));
    }
  }, []);

  /**
   * Triggers checkout reducer when component is mounted
   */
  useEffect(() => {
    dispatch({
      type: 'CHECKOUT_DISPATCH',
      payload: { payment: false },
    });
  }, []);

  /**
   * Call `handleFetchContent` when component is mounted
   */
  useEffect(() => {
    handleFetchContent();
  }, [handleFetchContent]);

  /**
   * Sets `listFilter` local state when applying filters
   */
  useEffect(() => {
    setState((prevState) => {
      let list = listItem;

      if (filterName && filterType) {
        const opts = {
          category: () => list.filter((item) => item.category.name === filterName),
          make: () => list.filter((item) => item.make === filterName),
        };

        list = opts[filterType]();
      }

      return {
        ...prevState,
        listFilter: list,
      };
    });
  }, [filterName, filterType]);

  return (
    <div className="index">
      <Row gutter={16}>
        <Col md={14} xs={24}>
          <div className="index__list">
            <div className="index__filter">
              <Typography.Text>
                Filter
              </Typography.Text>
              <Filter name={filterName} />
            </div>
            <Suspense fallback={null}>
              <List
                list={listFilter}
                loading={loading}
                type="items"
              />
            </Suspense>
          </div>
        </Col>
        <Col md={10} xs={24}>
          <Suspense fallback={null}>
            <Cart isAuthenticated={isAuthenticated} />
          </Suspense>
        </Col>
      </Row>
    </div>
  );
}

IndexPage.propTypes = {
  /**
   * Customer authentication status
   */
  isAuthenticated: PropTypes.bool.isRequired,
};

export default IndexPage;
