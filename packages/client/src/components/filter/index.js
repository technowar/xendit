import React from 'react';
import { Tag } from 'antd';
import PropTypes from 'prop-types';
import { UseStateValue } from 'provider';
import './styles.css';

/**
 * Component for showing filter
 *
 * @component
 * @property    {String}    name    Name to display
 *
 * @example
 * const name = 'Phone';
 *
 * return (
 *  <Filter name={name} />
 * )
 */
function FilterComponent(props) {
  const { name } = props;
  const [, dispatch] = UseStateValue();

  /**
   * Handles the reset of filter store
   */
  function handleClickClose() {
    dispatch({
      type: 'FILTER_DISPATCH',
      payload: {
        name: null,
        type: null,
      },
    });
  }

  return (
    name && (
      <div className="filter">
        <Tag closable onClose={() => handleClickClose()}>
          {name}
        </Tag>
      </div>
    )
  );
}

FilterComponent.propTypes = {
  /**
   * Name to be displayed
   */
  name: PropTypes.string,
};

FilterComponent.defaultProps = {
  name: null,
};

export default FilterComponent;
