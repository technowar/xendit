import React, { Suspense, lazy } from 'react';
import { ShoppingCartOutlined } from '@ant-design/icons';
import {
  Avatar,
  Badge,
  Button,
  Divider,
  Typography,
} from 'antd';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { UseStateValue } from 'provider';
import { createCustomer } from 'services/customer';
import { createOrder } from 'services/order';
import {
  TAX,
  calculateTotal,
  formatCurrency,
  setLocalStorage,
} from 'utils';
import './styles.css';

const List = lazy(() => import('components/list'));

/**
 * Component for cart section
 *
 * @component
 * @property    {Boolean}     isAuthenticated     Customer authentication status
 * @property    {Object}      user                Customer details
 *
 * @example
 * const isAuthenticated = true;
 * const user = { email: 'a@a.com', 'abcde' };
 *
 * return (
 *  <Cart
 *    isAuthenticated={isAuthenticated}
 *    user={user}
 *  />
 * )
 */
function CartComponent(props) {
  const { isAuthenticated, user } = props;
  const navigate = useNavigate();
  const [{
    cart: { list: listCart, size: listSize },
    checkout: { payment },
  }, dispatch] = UseStateValue();
  const total = calculateTotal(listCart, TAX);
  const price = formatCurrency(total, 'USD');
  const buttonOpts = {
    false: () => (payment ? 'REGISTER AND PAY' : 'GUEST CHECKOUT'),
    true: () => (payment ? 'PAY' : 'CHECKOUT'),
  };

  /**
   * Handles checkout button click
   *
   * Redirects unauthorized customers to home page
   *
   * Create customer on guest checkout
   *
   * Create order entry
   *
   * Triggers alert reducer on success
   *
   * Redirects thank you page on success
   *
   * Triggers alert reducer on error
   */
  function handleClickCheckout(type) {
    const opts = {
      false: () => navigate('/checkout'),
      true: async () => {
        try {
          let customer = user;
          const items = listCart
            .map((item) => ({
              id: item._id,
              order: item.order,
              quantity: item.quantity - item.order,
            }));

          if (!isAuthenticated) {
            customer = await createCustomer(user);

            setLocalStorage('user', customer);
          }

          await createOrder({
            customer: customer._id,
            items,
            total,
          });

          dispatch({
            type: 'ALERT_DISPATCH',
            payload: {
              message: 'Orders are now being processed',
              type: 'success',
            },
          });

          navigate('/thankyou', { replace: true });
        } catch (err) {
          dispatch({
            type: 'ALERT_DISPATCH',
            payload: {
              message: 'Unable to proceed checkout',
              type: 'error',
            },
          });
        }
      },
    };

    return opts[type]();
  }

  return (
    <div className="cart">
      <div className="cart__title">
        <Typography.Title className="cart__title-text" level={3}>
          Cart
        </Typography.Title>
        <Badge count={listSize}>
          <Avatar shape="square" icon={<ShoppingCartOutlined />} />
        </Badge>
      </div>
      <div className="cart__list">
        <Suspense fallback={null}>
          <List list={listCart} type="cart" />
        </Suspense>
      </div>
      {listSize ? (
        <>
          <Divider />
          <div className="cart__info">
            <div className="cart__info--details">
              <Typography.Text>
                Tax
              </Typography.Text>
              <Typography.Text strong>
                {`${TAX}%`}
              </Typography.Text>
            </div>
            <div className="cart__info--details">
              <Typography.Text>
                Shipping
              </Typography.Text>
              <Typography.Text strong>
                FREE
              </Typography.Text>
            </div>
            <div className="cart__info--details">
              <Typography.Text>
                Total
              </Typography.Text>
              <Typography.Text strong>
                {price}
              </Typography.Text>
            </div>
          </div>
          <div className="cart__checkout">
            <Button
              className="cart__checkout-btn"
              onClick={() => handleClickCheckout(payment)}
              type="primary"
            >
              {buttonOpts[isAuthenticated](payment)}
            </Button>
          </div>
        </>
      ) : null}
    </div>
  );
}

CartComponent.propTypes = {
  /**
   * Customer authentication status
   */
  isAuthenticated: PropTypes.bool,
  /**
   * Customer details
   */
  user: PropTypes.instanceOf(Object),
};

CartComponent.defaultProps = {
  isAuthenticated: false,
  user: null,
};

export default CartComponent;
