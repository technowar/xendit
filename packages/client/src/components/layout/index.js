import React, { useEffect } from 'react';
import { Layout } from 'antd';
import { UseStateValue } from 'provider';
import { getLocalStorage } from 'utils';
import Alert from 'components/alert';
import Footer from './footer';
import Header from './header';

/**
 * Higher order component for layout
 *
 * @component
 * @property    {Object}    BaseComponent     Components passed
 */
function LayoutHOC(BaseComponent) {
  function LayoutComponent() {
    const [{
      alert,
      alert: { message },
    }, dispatch] = UseStateValue();
    const customer = getLocalStorage('user') || {};
    const isAuthenticated = !!Object.keys(customer).length;
    const list = getLocalStorage('cart') || [];
    const size = list.reduce((acc, curr) => (curr.order + acc), 0);
    const props = {
      customer,
      isAuthenticated,
      list,
      size,
    };

    /**
     * Triggers cart reducer when component is mounted
     */
    useEffect(() => {
      dispatch({
        type: 'CART_DISPATCH',
        payload: { list, size },
      });
    }, []);

    return (
      <Layout>
        <Header isAuthenticated={isAuthenticated} layout={Layout} />
        <Layout.Content>
          {message && <Alert {...alert} />}
          <BaseComponent {...props} />
        </Layout.Content>
        <Footer layout={Layout} />
      </Layout>
    );
  }

  return LayoutComponent;
}

export default LayoutHOC;
