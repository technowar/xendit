import React from 'react';
import { ShoppingCartOutlined, UserOutlined } from '@ant-design/icons';
import {
  Avatar,
  Badge,
  Menu,
} from 'antd';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { UseStateValue } from 'provider';
import './styles.css';

/**
 * Component for header
 *
 * @component
 * @property    {Boolean}     isAuthenticated     Customer authentication status
 * @property    {Object}      layout              Layout from Antd
 *
 * @example
 * const { Layout } from 'antd';
 * const isAuthenticated = true;
 * const layout = Layout;
 *
 * return (
 *  <Header
 *    isAuthenticated={isAuthenticated}
 *    layout={Layout}
 *  />
 * )
 */
function HeaderComponent(props) {
  const { isAuthenticated, layout } = props;
  const navigate = useNavigate();
  const [{
    cart: { size: listSize },
  }] = UseStateValue();

  /**
   * Handles menu button click
   *
   * Redirects customer to checkout page
   *
   * Redirects customer to homge page
   *
   * Redirects customer to profile or signin page
   */
  function handleClick(type) {
    const opts = {
      cart: () => navigate('/checkout'),
      logo: () => navigate('/'),
      profile: () => (isAuthenticated ? navigate('/profile') : navigate('/signin')),
    };

    return opts[type]();
  }

  return (
    <layout.Header className="header">
      <div
        aria-hidden="true"
        className="header__logo"
        onClick={() => handleClick('logo')}
      />
      <Menu
        className="header__menu"
        mode="horizontal"
        theme="dark"
      >
        {(isAuthenticated || listSize) ? (
          <Menu.Item
            onClick={() => handleClick('cart')}
            icon={(
              <Badge count={listSize}>
                <Avatar icon={<ShoppingCartOutlined />} />
              </Badge>
            )}
            key="cart"
          />
        ) : null}
        <Menu.Item
          onClick={() => handleClick('profile')}
          icon={<Avatar icon={<UserOutlined />} />}
          key="profile"
        />
      </Menu>
    </layout.Header>
  );
}

HeaderComponent.propTypes = {
  /**
   * Customer authentication status
   */
  isAuthenticated: PropTypes.bool.isRequired,
  /**
   * Layout from Antd
   */
  layout: PropTypes.instanceOf(Object).isRequired,
};

export default HeaderComponent;
