import React from 'react';
import PropTypes from 'prop-types';

/**
 * Component for footer
 *
 * @component
 * @property    {Object}    layout   Layout from Antd
 *
 * @example
 * const { Layout } from 'antd';
 * const layout = Layout;
 *
 * return (
 *  <Footer layout={Layout} />
 * )
 */
function FooterComponent({ layout }) {
  return (
    <layout.Footer />
  );
}

FooterComponent.propTypes = {
  /**
   * Layout from Antd
   */
  layout: PropTypes.instanceOf(Object).isRequired,
};

export default FooterComponent;
