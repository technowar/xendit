import React, { Suspense, lazy } from 'react';
import { List } from 'antd';
import PropTypes from 'prop-types';

const ListCart = lazy(() => import('./cart'));
const ListItems = lazy(() => import('./items'));
const ListOrders = lazy(() => import('./orders'));
const items = {
  cart: (item, loading) => <ListCart item={item} loading={loading} />,
  items: (item, loading) => <ListItems item={item} loading={loading} />,
  orders: (item, loading) => <ListOrders item={item} loading={loading} />,
};

/**
 * Component for display list
 *
 * @component
 * @property    {Array}       list        List to be displayed
 * @property    {Boolean}     loading     Loading
 * @property    {String}      type        Type of list
 *
 * @example
 * const list = [];
 * const loading = true;
 * const type = 'items';
 *
 * return (
 *  <List
 *    list={list}
 *    loading={loading}
 *    type={type}
 *  />
 * )
 */
function ListComponent(props) {
  const {
    list,
    loading,
    type,
  } = props;

  return (
    <List
      className="list"
      dataSource={list}
      itemLayout="vertical"
      size="large"
      renderItem={(item) => (
        <Suspense fallback={null}>
          {items[type](item, loading)}
        </Suspense>
      )}
    />
  );
}

ListComponent.propTypes = {
  /**
   * List to be displayed
   */
  list: PropTypes.instanceOf(Array).isRequired,
  /**
   * Loading
   */
  loading: PropTypes.bool,
  /**
   * Type of list
   */
  type: PropTypes.oneOf(['cart', 'items', 'orders']),
};

ListComponent.defaultProps = {
  loading: false,
  type: 'items',
};

export default ListComponent;
