import React from 'react';
import {
  Avatar,
  List,
  Skeleton,
  Typography,
} from 'antd';
import PropTypes from 'prop-types';
import { formatCurrency } from 'utils';

/**
 * Component for display list
 *
 * @component
 * @property    {Object}      item        Item to be displayed
 * @property    {Boolean}     loading     Loading
 *
 * @example
 * const item = {};
 * const loading = true;
 *
 * return (
 *  <ListOrders
 *    item={item}
 *    loading={loading}
 *  />
 * )
 */
function ListOrdersComponent(props) {
  const {
    item,
    loading,
  } = props;

  return (
    <List.Item className="list__orders">
      <Skeleton
        active
        avatar
        loading={loading}
      >
        <>
          {item.orders.map((itemTemp) => (
            <List.Item.Meta
              avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
              className="list__orders--meta"
              description={(
                <div className="list__orders--meta-description">
                  <div className="list__orders--meta-description-details">
                    <Typography.Text>
                      {'Make: '}
                    </Typography.Text>
                    <Typography.Text strong>
                      {itemTemp.item.make}
                    </Typography.Text>
                  </div>
                  <div className="list__orders--meta-description-details">
                    <Typography.Text>
                      {'Order: '}
                    </Typography.Text>
                    <Typography.Text strong>
                      {itemTemp.order}
                    </Typography.Text>
                  </div>
                  <div className="list__orders--meta-description-details">
                    <Typography.Text>
                      {'Price: '}
                    </Typography.Text>
                    <Typography.Text strong>
                      {formatCurrency(itemTemp.order * itemTemp.item.price, 'USD')}
                    </Typography.Text>
                  </div>
                </div>
              )}
              key={itemTemp._id}
              title={(
                <Typography.Link href={`/items/${itemTemp._id}`}>
                  {itemTemp.item.product}
                </Typography.Link>
              )}
            />
          ))}
          <Typography.Text strong>
            {`TOTAL: ${formatCurrency(item.total, 'USD')}`}
          </Typography.Text>
        </>
      </Skeleton>
    </List.Item>
  );
}

ListOrdersComponent.propTypes = {
  /**
   * Item to be displayed
   */
  item: PropTypes.instanceOf(Object).isRequired,
  /**
   * Loading
   */
  loading: PropTypes.bool.isRequired,
};

export default ListOrdersComponent;
