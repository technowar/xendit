import React from 'react';
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import {
  Avatar,
  Badge,
  Button,
  List,
  Skeleton,
  Typography,
} from 'antd';
import PropTypes from 'prop-types';
import { UseStateValue } from 'provider';
import { formatCurrency } from 'utils';
import './styles.css';

/**
 * Component for display list
 *
 * @component
 * @property    {Object}      item        Item to be displayed
 * @property    {Boolean}     loading     Loading
 *
 * @example
 * const item = {};
 * const loading = true;
 *
 * return (
 *  <ListCart
 *    item={item}
 *    loading={loading}
 *  />
 * )
 */
function ListCartComponent(props) {
  const {
    item,
    loading,
  } = props;
  const [{
    checkout: { payment },
  }, dispatch] = UseStateValue();
  const price = formatCurrency(item.price * item.order, 'USD');

  /**
   * Handles add or delete button click
   *
   * Triggers cart reducer on delete item
   *
   * Triggers alert reducer on empty cart
   *
   * Triggers cart reducer on add item
   */
  function handleClickButton(action) {
    const opts = {
      false: () => {
        dispatch({
          type: item.order === 1 ? 'CART_REMOVE' : 'CART_SUBTRACT',
          payload: { item },
        });

        if (item.order === 1) {
          dispatch({
            type: 'ALERT_DISPATCH',
            payload: {
              message: `${item.make} ${item.product} removed from cart`,
              type: 'info',
            },
          });
        }
      },
      true: () => {
        dispatch({
          type: 'CART_ADD',
          payload: { item },
        });
      },
    };

    return opts[action]();
  }

  /**
   * Handles filter button click
   *
   * Triggers filter reducer
   */
  function handleClickFilter(name, type) {
    dispatch({
      type: 'FILTER_DISPATCH',
      payload: { name, type },
    });
  }

  return (
    <List.Item
      actions={!payment && [
        <>
          <Button
            danger
            icon={<MinusCircleOutlined />}
            onClick={() => handleClickButton(false)}
            shape="circle"
            type="default"
          />
          <Button
            icon={<PlusCircleOutlined />}
            onClick={() => handleClickButton(true)}
            shape="circle"
            type="primary"
          />
        </>,
      ]}
      className="list__cart"
    >
      <Skeleton
        active
        avatar
        loading={loading}
      >
        <List.Item.Meta
          avatar={(
            <Badge count={item.order}>
              <Avatar src="https://joeschmoe.io/api/v1/random" />
            </Badge>
          )}
          className="list__cart--meta"
          description={(
            <div className="list__cart--meta-description">
              <div className="list__cart--meta-description-details">
                <Typography.Text>
                  {'Make: '}
                </Typography.Text>
                <Typography.Text strong>
                  {payment ? item.make : (
                    <Typography.Link onClick={() => handleClickFilter(item.make, 'make')}>
                      {item.make}
                    </Typography.Link>
                  )}
                </Typography.Text>
              </div>
              <div className="list__cart--meta-description-details">
                <Typography.Text>
                  {'Category: '}
                </Typography.Text>
                <Typography.Text strong>
                  {payment ? item.category.name : (
                    <Typography.Link onClick={() => handleClickFilter(item.category.name, 'category')}>
                      {item.category.name}
                    </Typography.Link>
                  )}
                </Typography.Text>
              </div>
              <div className="list__cart--meta-description-details">
                <Typography.Text>
                  {'Total: '}
                </Typography.Text>
                <Typography.Text strong>
                  {price}
                </Typography.Text>
              </div>
              <div className="list__cart--meta-description-details">
                <Typography.Text>
                  {'Stock: '}
                </Typography.Text>
                <Typography.Text strong>
                  {item.quantity}
                </Typography.Text>
              </div>
            </div>
          )}
          title={(
            <Typography.Link href={`/items/${item._id}`}>
              {item.product}
            </Typography.Link>
          )}
        />
      </Skeleton>
    </List.Item>
  );
}

ListCartComponent.propTypes = {
  /**
   * Item to be displayed
   */
  item: PropTypes.instanceOf(Object).isRequired,
  /**
   * Loading
   */
  loading: PropTypes.bool.isRequired,
};

export default ListCartComponent;
