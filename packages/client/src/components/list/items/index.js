import React from 'react';
import { ShoppingCartOutlined } from '@ant-design/icons';
import {
  Avatar,
  Button,
  List,
  Skeleton,
  Typography,
} from 'antd';
import PropTypes from 'prop-types';
import { UseStateValue } from 'provider';
import { formatCurrency } from 'utils';
import './styles.css';

/**
 * Component for display list
 *
 * @component
 * @property    {Object}      item        Item to be displayed
 * @property    {Boolean}     loading     Loading
 *
 * @example
 * const item = {};
 * const loading = true;
 *
 * return (
 *  <ListItems
 *    item={item}
 *    loading={loading}
 *  />
 * )
 */
function ListItemsComponent(props) {
  const {
    item,
    loading,
  } = props;
  const [, dispatch] = UseStateValue();
  const price = formatCurrency(item.price, 'USD');

  /**
   * Handles add button click
   *
   * Triggers alert reducer on add item
   *
   * Triggers cart reducer on add item
   */
  function handleClickButton() {
    const {
      created,
      upc,
      ...rest
    } = item;

    dispatch({
      type: 'ALERT_DISPATCH',
      payload: {
        message: `${item.make} ${item.product} added to cart`,
        type: 'info',
      },
    });
    dispatch({
      type: 'CART_ADD',
      payload: { item: rest },
    });
  }

  /**
   * Handles filter button click
   *
   * Triggers filter reducer
   */
  function handleClickFilter(name, type) {
    dispatch({
      type: 'FILTER_DISPATCH',
      payload: { name, type },
    });
  }

  return (
    <List.Item
      actions={!loading && [
        <Button
          disabled={!item.quantity}
          icon={<ShoppingCartOutlined />}
          onClick={() => handleClickButton()}
          shape="round"
          size="small"
          type="default"
        />,
      ]}
      className="list__items"
    >
      <Skeleton
        active
        avatar
        loading={loading}
      >
        <List.Item.Meta
          avatar={
            <Avatar src="https://joeschmoe.io/api/v1/random" />
          }
          className="list__items--meta"
          description={(
            <div className="list__items--meta-description">
              <div className="list__items--meta-description-details">
                <Typography.Text>
                  {'Make: '}
                </Typography.Text>
                <Typography.Text strong>
                  <Typography.Link onClick={() => handleClickFilter(item.make, 'make')}>
                    {item.make}
                  </Typography.Link>
                </Typography.Text>
              </div>
              <div className="list__items--meta-description-details">
                <Typography.Text>
                  {'Category: '}
                </Typography.Text>
                <Typography.Text strong>
                  <Typography.Link onClick={() => handleClickFilter(item.category.name, 'category')}>
                    {item.category.name}
                  </Typography.Link>
                </Typography.Text>
              </div>
              <div className="list__items--meta-description-details">
                <Typography.Text>
                  {'Price: '}
                </Typography.Text>
                <Typography.Text strong>
                  {price}
                </Typography.Text>
              </div>
              <div className="list__items--meta-description-details">
                <Typography.Text>
                  {'Stock: '}
                </Typography.Text>
                <Typography.Text strong>
                  {item.quantity}
                </Typography.Text>
              </div>
            </div>
          )}
          title={(
            <Typography.Link href={`/items/${item._id}`}>
              {item.product}
            </Typography.Link>
          )}
        />
      </Skeleton>
    </List.Item>
  );
}

ListItemsComponent.propTypes = {
  /**
   * Item to be displayed
   */
  item: PropTypes.instanceOf(Object).isRequired,
  /**
   * Loading
   */
  loading: PropTypes.bool.isRequired,
};

export default ListItemsComponent;
