import React from 'react';
import { Alert } from 'antd';
import PropTypes from 'prop-types';
import { UseStateValue } from 'provider';
import './styles.css';

/**
 * Component for showing alert messages
 *
 * @component
 * @property    {String}    message   Message to display
 * @property    {String}    type      Type of alert to use
 *
 * @example
 * const messge = 'This is a message';
 * const type = 'success';
 *
 * return (
 *  <Alert
 *    message={message}
 *    type={type}
 *  />
 * )
 */
function AlertComponent(props) {
  const { message, type } = props;
  const [, dispatch] = UseStateValue();

  /**
   * Handles the reset of alert store
   */
  function handleClickClose() {
    dispatch({
      type: 'ALERT_DISPATCH',
      payload: {
        message: null,
        type: null,
      },
    });
  }

  return (
    <div className="alert">
      <div className="alert__message">
        <Alert
          closable
          message={message}
          onClose={() => handleClickClose()}
          type={type}
        />
      </div>
    </div>
  );
}

AlertComponent.propTypes = {
  /**
   * Message to be displayed
   */
  message: PropTypes.string.isRequired,
  /**
   * Type to be used
   */
  type: PropTypes.string.isRequired,
};

export default AlertComponent;
