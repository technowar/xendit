import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

const InitialState = {
  alert: {
    message: null,
    type: null,
  },
  cart: {
    list: [],
    size: 0,
  },
  checkout: {
    payment: false,
  },
  filter: {
    name: null,
    type: null,
  },
};
const Store = createContext();
const UseStateValue = () => useContext(Store);

function StateProvider({ reducers, initialState, children }) {
  return (
    <Store.Provider value={useReducer(reducers, initialState)}>
      {children}
    </Store.Provider>
  );
}

StateProvider.propTypes = {
  reducers: PropTypes.instanceOf(Object).isRequired,
  initialState: PropTypes.instanceOf(Object).isRequired,
  children: PropTypes.instanceOf(Object).isRequired,
};

export {
  InitialState,
  Store,
  StateProvider,
  UseStateValue,
};
