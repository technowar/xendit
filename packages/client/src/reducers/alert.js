/**
 * @module reducers/alert
 */

/**
 * Reducer for alert
 *
 * @param     {Object}    state
 * @param     {Object}    action
 *
 * @return    {Object}    Alert states
 */
export default (state, action) => {
  switch (action.type) {
    case 'ALERT_DISPATCH':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
