/**
 * @module reducers/filter
 */

/**
 * Reducer for filter
 *
 * @param     {Object}    state
 * @param     {Object}    action
 *
 * @return    {Object}    Filter states
 */
export default (state, action) => {
  switch (action.type) {
    case 'FILTER_DISPATCH':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
