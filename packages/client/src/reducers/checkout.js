/**
 * @module reducers/checkout
 */

/**
 * Reducer for checkout
 *
 * @param     {Object}    state
 * @param     {Object}    action
 *
 * @return    {Object}    Checkout states
 */
export default (state, action) => {
  switch (action.type) {
    case 'CHECKOUT_DISPATCH':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
