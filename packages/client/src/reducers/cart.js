/**
 * @module reducers/cart
 */

import { setLocalStorage } from 'utils';

/**
 * Reducer for cart
 *
 * @param     {Object}    state
 * @param     {Object}    action
 *
 * @return    {Object}    Cart states
 */
export default (state, action) => {
  const { list } = state;

  switch (action.type) {
    case 'CART_ADD': {
      const {
        payload: { item },
      } = action;
      const itemIds = list.map((itemTemp) => itemTemp._id);
      const itemIdx = itemIds.indexOf(item._id);

      if (item.quantity) {
        if (itemIdx === -1) {
          list.push({
            ...item,
            order: 1,
          });
        } else {
          const itemTemp = list[itemIdx];

          if (itemTemp.order < itemTemp.quantity) {
            list[itemIdx] = {
              ...item,
              order: itemTemp.order + 1,
            };
          }
        }
      }

      const size = list.reduce((acc, curr) => (curr.order + acc), 0);

      setLocalStorage('cart', list);

      return {
        ...state,
        list,
        size,
      };
    }
    case 'CART_REMOVE': {
      const {
        payload: { item },
      } = action;
      const updatedList = list.filter((itemTemp) => itemTemp._id !== item._id);
      const size = updatedList.reduce((acc, curr) => (curr.order + acc), 0);

      setLocalStorage('cart', updatedList);

      return {
        ...state,
        list: updatedList,
        size,
      };
    }
    case 'CART_SUBTRACT': {
      const {
        payload: { item },
      } = action;
      const itemIds = list.map((itemTemp) => itemTemp._id);
      const itemIdx = itemIds.indexOf(item._id);
      const itemTemp = list[itemIdx];

      list[itemIdx] = {
        ...item,
        order: itemTemp.order - 1,
      };

      const size = list.reduce((acc, curr) => (curr.order + acc), 0);

      setLocalStorage('cart', list);

      return {
        ...state,
        list,
        size,
      };
    }
    case 'CART_DISPATCH':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
