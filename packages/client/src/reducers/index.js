import AlertReducer from './alert';
import CartReducer from './cart';
import CheckoutReducer from './checkout';
import FilterReducer from './filter';

export default ({
  alert,
  cart,
  checkout,
  filter,
}, action) => ({
  alert: AlertReducer(alert, action),
  cart: CartReducer(cart, action),
  checkout: CheckoutReducer(checkout, action),
  filter: FilterReducer(filter, action),
});
