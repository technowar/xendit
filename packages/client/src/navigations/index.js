import React from 'react';
import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
} from 'react-router-dom';
import Layout from 'components/layout';
import Checkout from 'pages/checkout';
import Index from 'pages/index';
import Item from 'pages/item';
import Profile from 'pages/profile';
import SignIn from 'pages/signin';
import ThankYou from 'pages/thankyou';

const CheckoutPage = Layout(Checkout);
const IndexPage = Layout(Index);
const ItemPage = Layout(Item);
const ProfilePage = Layout(Profile);
const SignInPage = Layout(SignIn);
const ThankYouPage = Layout(ThankYou);

function Navigations() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          exact
          path="/"
          element={<IndexPage />}
        />
        <Route
          exact
          path="items/:id"
          element={<ItemPage />}
        />
        <Route
          exact
          path="checkout"
          element={<CheckoutPage />}
        />
        <Route
          exact
          path="profile"
          element={<ProfilePage />}
        />
        <Route
          exact
          path="signin"
          element={<SignInPage />}
        />
        <Route
          exact
          path="thankyou"
          element={<ThankYouPage />}
        />
        <Route
          path="*"
          element={<Navigate to="/" />}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default Navigations;
