import axios from 'axios';

const client = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
  withCredentials: true,
});

client.interceptors.request.use((config) => config, (error) => Promise.reject(error));

client.interceptors.response.use((response) => {
  const {
    data: {
      data,
      message,
      status,
    },
  } = response;

  if (status === 'error') {
    throw new Error(message);
  }

  return data;
}, (error) => Promise.reject(error));

export default client;
