import client from './index';

/**
 * Returns created order
 *
 * @param     {Object}    params    Order payload
 *
 * @return    {Promise}             Order details
 */
function createOrder(params) {
  return client.post('/orders', params);
}

/**
 * Returns list of orders
 *
 * @param     {Object}    opts    Options to be used as query strings
 *
 * @return    {Promise}           List of orders
 */
function getOrders(opts) {
  const params = new URLSearchParams(opts).toString();

  return client.get(`/orders?${params}`);
}

export {
  createOrder,
  getOrders,
};
