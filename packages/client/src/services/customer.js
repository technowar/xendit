import client from './index';

/**
 * Returns authenticated customer details
 *
 * @param     {Object}    params    Customer payload
 *
 * @return    {Promise}             Customer details
 */
function authenticateCustomer(params) {
  return client.post('/signin', params);
}

/**
 * Returns created customer details
 *
 * @param     {Object}    params    Customer payload
 *
 * @return    {Promise}             Customer details
 */
function createCustomer(params) {
  return client.post('/customers', params);
}

export {
  authenticateCustomer,
  createCustomer,
};
