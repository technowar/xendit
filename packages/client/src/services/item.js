import client from './index';

/**
 * Returns specific item
 *
 * @param     {String}    id    Item ID to be fetched
 *
 * @return    {Promise}         Item details
 */
function getItem(id) {
  return client.get(`/items/${id}`);
}

/**
 * Returns list of items
 *
 * @param     {Object}    opts    Options to be used as query strings
 *
 * @return    {Promise}           List of items
 */
function getItems(opts) {
  const params = new URLSearchParams(opts).toString();

  return client.get(`/items?${params}`);
}

export {
  getItem,
  getItems,
};
