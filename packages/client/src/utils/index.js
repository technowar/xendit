/**
 * Current tax value
 *
 * @constant
 * @type {Number}
 * @default
 */
const TAX = 24;

/**
 * Returns formatted amount to selected currency
 *
 * @param     {Number}    amount    Amount to be formatted
 * @param     {String}    currency  Currency to use
 *
 * @return    {String}              Formatted amount
 */
function formatCurrency(amount, currency) {
  return new Intl.NumberFormat(
    'en-US',
    { style: 'currency', currency },
  ).format(amount);
}

/**
 * Returns grand total to be paid
 *
 * @param     {Array}     list    List of items
 * @param     {Number}    tax     Total tax
 *
 * @return    {Number}            Grand total
 */
function calculateTotal(list, tax) {
  const subtotal = list.reduce((acc, curr) => acc + (curr.price * curr.order), 0);
  const totalTax = subtotal * (tax / 100);
  const total = (subtotal + totalTax).toFixed(2);

  return total;
}

/**
 * Returns item from browser's local storage
 *
 * @param     {String}    item    Item to be retrieved
 *
 * @return    {Object}            Item from local storage
 */
function getLocalStorage(item) {
  return JSON.parse(localStorage.getItem(item));
}

/**
 * Remove item from browser's local storage
 *
 * @param     {String}    item    Item to be removed
 */
function removeLocalStorage(item) {
  localStorage.removeItem(item);
}

/**
 * Set data to browser's local storage
 *
 * @param     {String}    item    Name of the data to be set
 * @param     {Object}    data    Data to be set
 */
function setLocalStorage(item, data) {
  localStorage.setItem(item, JSON.stringify(data));
}

export {
  TAX,

  calculateTotal,
  formatCurrency,
  getLocalStorage,
  removeLocalStorage,
  setLocalStorage,
};
